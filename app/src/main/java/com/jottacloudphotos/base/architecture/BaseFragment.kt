package com.jottacloudphotos.base.architecture

import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {

    private val minimizeBackPressedCallback by lazy {
        object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                requireActivity().moveTaskToBack(true)
            }
        }
    }


    protected fun minimizeOnBackPressed() {
        viewLifecycleOwnerLiveData.observe(this) {
            requireActivity().onBackPressedDispatcher.addCallback(it, minimizeBackPressedCallback)
        }
    }

    protected fun onBackPressed() {
        requireActivity().onBackPressed()
    }

    protected fun doOnBackPressed(removeCallbackAfterClick: Boolean = true, action: () -> Unit) {
        activity?.onBackPressedDispatcher?.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    action()
                    if (removeCallbackAfterClick) this.remove()
                }
            }
        )
    }

    override fun onDestroy() {
        minimizeBackPressedCallback.remove()
        super.onDestroy()
    }
}

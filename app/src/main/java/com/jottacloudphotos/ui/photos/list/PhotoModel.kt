package com.jottacloudphotos.ui.photos.list

import com.jottacloudphotos.ui.photos.PhotoSize

data class PhotoModel(
    val title: String?,
    val url: String?,
    val thumbnailUrl: String,
    val id: String
) {
    val thumbnailLargeUrl = thumbnailUrl + PhotoSize.LARGE.suffixToThumbnail
    val thumbnailMediumUrl = thumbnailUrl + PhotoSize.MEDIUM.suffixToThumbnail

}
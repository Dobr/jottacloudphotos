package com.jottacloudphotos.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import com.jottacloudphotos.R
import com.jottacloudphotos.base.activity.NavControllerProvider
import com.jottacloudphotos.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), NavControllerProvider {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

    }

    override val navController by lazy {
        Navigation.findNavController(this, R.id.nav_host_fragment)
    }
}
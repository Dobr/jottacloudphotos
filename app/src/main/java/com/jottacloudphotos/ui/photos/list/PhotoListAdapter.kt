package com.jottacloudphotos.ui.photos.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import androidx.databinding.DataBindingUtil
import com.jottacloudphotos.R
import com.jottacloudphotos.databinding.PhotoAlbumItemBinding


class PhotoListAdapter(val callback: PhotoAdapterCallback) :
    RecyclerView.Adapter<PhotoListAdapter.PhotoViewHolder>() {
    private var photos: List<PhotoModel>

    init {
        photos = emptyList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val itemPhotoBinding: PhotoAlbumItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.photo_album_item,
            parent, false
        )
        return PhotoViewHolder(itemPhotoBinding)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.bindPhoto(photos[position], callback)
    }

    override fun getItemCount(): Int {
        return photos.size
    }

    fun setPhotos(photos: List<PhotoModel>) {
        this.photos = photos
        notifyDataSetChanged()
    }


    class PhotoViewHolder(private val itemPhotoBinding: PhotoAlbumItemBinding) :
        RecyclerView.ViewHolder(itemPhotoBinding.root) {

        fun bindPhoto(photo: PhotoModel, callback: PhotoAdapterCallback) {
            itemPhotoBinding.photoItemViewModel = PhotoItemViewModel(photo)
            itemPhotoBinding.itemPhoto.setOnClickListener { callback.onPhotoItemClick(photo.id) }
        }

    }
}

interface PhotoAdapterCallback {
    fun onPhotoItemClick(id: String)
}
package com.jottacloudphotos.domain

import com.jottacloudphotos.ui.photos.list.PhotoModel
import java.io.Serializable

class AlbumModelDomain : Serializable {
    var photos: List<PhotoModel> = emptyList()
}
package com.jottacloudphotos.ui.photos

import android.os.Bundle
import androidx.navigation.fragment.findNavController
import com.jottacloudphotos.R
import com.jottacloudphotos.BR
import com.jottacloudphotos.base.architecture.MvvmFragment
import com.jottacloudphotos.databinding.PhotoAlbumFragmentBinding
import androidx.recyclerview.widget.GridLayoutManager
import com.jottacloudphotos.ui.photos.list.PhotoListAdapter

class PhotoAlbumFragment :
    MvvmFragment<PhotoAlbumFragmentBinding, PhotoAlbumUIEvent, PhotoAlbumViewModel>(
        R.layout.photo_album_fragment,
        PhotoAlbumViewModel::class.java,
        BR.viewModel
    ) {
    override fun onFragmentCreated(savedInstanceState: Bundle?) {
        binding.listPhoto.layoutManager = GridLayoutManager(requireContext(), getColumnCount())
        val adapter = PhotoListAdapter(viewModel)
        binding.listPhoto.adapter = adapter


    }

    override fun onNewUiEvent(uiEvent: PhotoAlbumUIEvent) {
        when (uiEvent) {
            is PhotoAlbumUIEvent.NavigateToPhotoOverview -> findNavController().navigate(
                PhotoAlbumFragmentDirections.actionPhotoAlbumFragmentToPhotoOverviewFragment(
                    uiEvent.album, uiEvent.photoId
                )
            )
        }
    }

    private fun getColumnCount(): Int {
        return resources.getInteger(R.integer.photos_columns)
    }
}
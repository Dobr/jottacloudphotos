package com.jottacloudphotos.network.data

import com.jottacloudphotos.network.model.Model
import kotlinx.coroutines.flow.Flow

interface PhotoAlbumRepository {
    suspend fun fetchPhotoAlbum(shareUri: String): Flow<Model.Album?>
}

package com.jottacloudphotos.use_cases

import com.jottacloudphotos.domain.AlbumModelDomain
import com.jottacloudphotos.network.data.PhotoAlbumRepository
import com.jottacloudphotos.ui.photos.list.PhotoModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

interface PhotoAlbumUseCases {
    suspend fun fetchSharedAlbums(shareUri: String): Flow<AlbumModelDomain>
}

class PhotoAlbumUseCasesImpl(private val photoAlbumRepository: PhotoAlbumRepository) :
    PhotoAlbumUseCases {

    override suspend fun fetchSharedAlbums(shareUri: String): Flow<AlbumModelDomain> {
        val albumLocal = AlbumModelDomain()
        return photoAlbumRepository.fetchPhotoAlbum(shareUri).map { album ->
            when (album?.photos?.isNotEmpty()) {
                true -> {
                    albumLocal.photos = album.photos.map { photo ->
                        PhotoModel(
                            photo.title,
                            photo.fileUrl,
                            photo.thumbnailUrl ?: "",
                            photo.id.toString()
                        )
                    }
                    albumLocal
                }
                false -> albumLocal
                else -> throw Throwable("error during fetching photo albums")
            }
        }

    }
}
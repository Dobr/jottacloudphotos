package com.jottacloudphotos.base.ui

import androidx.lifecycle.MutableLiveData

open class NonNullMutableLiveData<T>(
    value: T,
    val default: T = value
) : MutableLiveData<T>() {

    init {
        this.value = value
    }

    @Suppress("RedundantOverride")
    override fun setValue(value: T) = super.setValue(value)

    override fun getValue(): T {
        return super.getValue()!!
    }

    fun resetToDefault() {
        value = default
    }
}

package com.jottacloudphotos.network.data

import com.jottacloudphotos.network.data.source.PhotoAlbumNetworkSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class PhotoAlbumRepositoryImpl(
    private val networkSource: PhotoAlbumNetworkSource
) : PhotoAlbumRepository {

    override suspend fun fetchPhotoAlbum(shareUri: String) = flow {
        val response = networkSource.fetchPhotoAlbum(shareUri)
        emit(response)
    }.flowOn(Dispatchers.IO)

}

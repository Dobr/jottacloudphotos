package com.jottacloudphotos.ui.overview

import android.os.Bundle
import androidx.navigation.fragment.navArgs
import com.jottacloudphotos.BR
import com.jottacloudphotos.R
import com.jottacloudphotos.base.architecture.MvvmFragment
import com.jottacloudphotos.base.architecture.UiEvent
import com.jottacloudphotos.databinding.PhotoOverviewFragmentBinding


class PhotoOverviewFragment :
    MvvmFragment<PhotoOverviewFragmentBinding, UiEvent, OverviewViewModel>(
        R.layout.photo_overview_fragment,
        OverviewViewModel::class.java,
        BR.viewModel
    ) {
    private val args: PhotoOverviewFragmentArgs by navArgs()

    override fun onFragmentCreated(savedInstanceState: Bundle?) {
        viewModel.showInitImage(args.photoId, args.album)
    }


    override fun onNewUiEvent(uiEvent: UiEvent) {
        TODO("Not yet implemented")
    }

}
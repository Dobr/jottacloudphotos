package com.jottacloudphotos.base.architecture

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jottacloudphotos.base.ui.EventLiveData
import com.jottacloudphotos.base.ui.NonNullMutableLiveData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber

abstract class BaseViewModel<E : UiEvent> : ViewModel() {
    val uiEvents = EventLiveData<E>()
    val errors = EventLiveData<String>()

    fun postUiEvent(uiEvent: E) {
        uiEvents.postAsEvent(uiEvent)
    }

    fun postError(error: String) {
        errors.postAsEvent(error)
    }

    fun <T> doAsyncWork(
        work: suspend () -> Flow<T>,
        onSuccess: (T) -> Unit,
        onFailureMessage: String? = null,
        onFailure: (Throwable) -> Unit = { doOnError(it, onFailureMessage) },
        progressLiveData: NonNullMutableLiveData<Boolean>? = null
    ) {
        viewModelScope.launch {
            work().let { resultFlow ->
                if (progressLiveData != null) {
                    resultFlow.applyLoadingStatusInto(progressLiveData)
                } else {
                    resultFlow
                }
            }.catch {
                Timber.e(it)
                onFailure(it)
            }.collect {
                onSuccess(it)
            }
        }
    }

    open fun doOnError(throwable: Throwable, errorMessage: String? = null) {
        //  Override to change exception handling (e.g. open error screen)
        (errorMessage ?: throwable.message)?.let {
            errors.postAsEvent(it)
        }
    }
}

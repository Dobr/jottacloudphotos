package com.jottacloudphotos.base.activity

import androidx.navigation.NavController

interface NavControllerProvider {
    val navController: NavController
}


package com.jottacloudphotos

import android.app.Application
import com.jottacloudphotos.di.photoAlbumModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class JottaPhotosApp : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@JottaPhotosApp)
            modules(koinModules)
        }
    }
}

private val koinModules = listOf(
    photoAlbumModule
)
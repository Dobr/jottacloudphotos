package com.jottacloudphotos.ui.overview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jottacloudphotos.base.architecture.BaseViewModel
import com.jottacloudphotos.base.architecture.UiEvent
import com.jottacloudphotos.domain.AlbumModelDomain

class OverviewViewModel : BaseViewModel<UiEvent>(), OverviewTouchListener {
    private val _imageUrl = MutableLiveData<String>()
    val imageUrl: LiveData<String> get() = _imageUrl
    private lateinit var album: AlbumModelDomain
    private var imageIndex = 0
    var onTouchListener: OverviewTouchListener = this@OverviewViewModel

    fun showInitImage(photoId: String, album: AlbumModelDomain?) {
        if (album != null) {
            this.album = album
        }
        val imageUrl =
            album?.photos?.find { it.id == photoId }?.thumbnailLargeUrl

        imageUrl.let { _imageUrl.postValue(it) }

        imageIndex = album?.photos?.indexOf(album.photos.find { it.id == photoId }) ?: 0

    }

    private fun showNextImage() {
        if (imageIndex == album.photos.size - 1) {
            imageIndex = 0
        }
        _imageUrl.postValue(album.photos[imageIndex++].thumbnailLargeUrl)

    }

    private fun showPreviousImage() {
        if (imageIndex == 0) {
            imageIndex = album.photos.size
        }
        _imageUrl.postValue(album.photos[imageIndex--].thumbnailLargeUrl)

    }

    override fun onLeft() {
        showPreviousImage()
    }

    override fun onRight() {
        showNextImage()
    }
}

interface OverviewTouchListener {
    fun onLeft()
    fun onRight()

}
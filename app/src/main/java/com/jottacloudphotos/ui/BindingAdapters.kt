package com.jottacloudphotos.ui

import android.annotation.SuppressLint
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jottacloudphotos.domain.AlbumModelDomain
import com.jottacloudphotos.ui.overview.OverviewTouchListener
import com.jottacloudphotos.ui.overview.OverviewViewModel
import com.jottacloudphotos.ui.photos.list.PhotoListAdapter

@BindingAdapter("image")
fun loadImage(view: ImageView?, url: String?) {
    if (view != null) {
        Glide
            .with(view.context)
            .load(url)
            .centerCrop()
            .into(view)
    }
}

@BindingAdapter("items")
fun loadItems(view: RecyclerView?, album: AlbumModelDomain?) {
    if (view?.adapter is PhotoListAdapter) {
        album?.photos?.let { (view.adapter as PhotoListAdapter).setPhotos(it) }
    }
}

@SuppressLint("ClickableViewAccessibility")
@BindingAdapter("onTouchListener")
fun setOnTouchListener(view: View, onTouchListener: OverviewTouchListener?) {
    lateinit var gestureDetector: GestureDetector
    val swipeThreshold = 100
    val swipeVelocityThreshold = 100
    var scaleFactor = 1f
    val scaleGestureDetector = ScaleGestureDetector(
        view.context,
        object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
            override fun onScale(detector: ScaleGestureDetector): Boolean {
                scaleFactor *= detector.scaleFactor
                scaleFactor = scaleFactor.coerceIn(0.1f, 4.0f)

                view.scaleX = scaleFactor
                view.scaleY = scaleFactor

                return super.onScale(detector)
            }
        }

    )
    gestureDetector =
        GestureDetector(view.context,
            object : GestureDetector.OnGestureListener {
                override fun onDown(p0: MotionEvent?): Boolean {
                    return false

                }

                override fun onShowPress(p0: MotionEvent?) {

                }

                override fun onSingleTapUp(p0: MotionEvent?): Boolean {
                    return false

                }

                override fun onScroll(
                    p0: MotionEvent?,
                    p1: MotionEvent?,
                    p2: Float,
                    p3: Float
                ): Boolean {
                    return false

                }

                override fun onLongPress(p0: MotionEvent?) {
                }

                override fun onFling(
                    e1: MotionEvent,
                    e2: MotionEvent,
                    velocityX: Float,
                    velocityY: Float
                ): Boolean {
                    try {
                        val diffY = e2.y - e1.y
                        val diffX = e2.x - e1.x
                        if (Math.abs(diffX) > Math.abs(diffY)) {
                            if (Math.abs(diffX) > swipeThreshold && Math.abs(velocityX) > swipeVelocityThreshold) {
                                if (diffX > 0) {
                                    onTouchListener?.onRight()
                                } else {
                                    onTouchListener?.onLeft()
                                }
                            }
                        }
                    } catch (exception: Exception) {
                        exception.printStackTrace()
                    }
                    return true
                }

            })

    view.setOnTouchListener { _, event ->
        if (gestureDetector.onTouchEvent(event)) {
            true

        } else {
            scaleGestureDetector.onTouchEvent(event)
        }
    }

}
package com.jottacloudphotos.base.architecture

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import org.koin.android.viewmodel.ext.android.getSharedViewModel
import org.koin.android.viewmodel.ext.android.getViewModel

abstract class BaseViewModelBindingFragment<out VM : ViewModel, VDB : ViewDataBinding>(
    @LayoutRes private val layoutId: Int,
    vmKClass: Class<out VM>,
    private val viewModelVariableInt: Int,
    sharedVM: Boolean = false
) : BaseBindingFragment<VDB>(layoutId) {

    protected val viewModel: VM by lazy {
        if (sharedVM) getSharedViewModel(clazz = vmKClass.kotlin)
        else getViewModel(clazz = vmKClass.kotlin)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.setVariable(viewModelVariableInt, viewModel)
        return binding.root
    }
}

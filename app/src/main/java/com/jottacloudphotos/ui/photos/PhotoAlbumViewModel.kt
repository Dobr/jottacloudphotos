package com.jottacloudphotos.ui.photos

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jottacloudphotos.base.architecture.BaseViewModel
import com.jottacloudphotos.base.architecture.UiEvent
import com.jottacloudphotos.domain.AlbumModelDomain
import com.jottacloudphotos.ui.photos.PhotoAlbumUIEvent.Companion.SHARE_URI_EXAMPLE_1
import com.jottacloudphotos.ui.photos.list.PhotoAdapterCallback
import com.jottacloudphotos.use_cases.PhotoAlbumUseCases

class PhotoAlbumViewModel(private val photoAlbumUseCases: PhotoAlbumUseCases) :
    BaseViewModel<PhotoAlbumUIEvent>(), PhotoAdapterCallback {
    private val _album = MutableLiveData<AlbumModelDomain>()
    val albumLiveData: LiveData<AlbumModelDomain> get() = _album

    init {
        doAsyncWork(
            work = { photoAlbumUseCases.fetchSharedAlbums(SHARE_URI_EXAMPLE_1) },
            onSuccess = { _album.postValue(it) }
        )
    }


    override fun onPhotoItemClick(id: String) {
        postUiEvent(PhotoAlbumUIEvent.NavigateToPhotoOverview(id, albumLiveData.value))
    }
}

sealed class PhotoAlbumUIEvent : UiEvent() {
    data class NavigateToPhotoOverview(val photoId: String, val album: AlbumModelDomain?) :
        PhotoAlbumUIEvent()

    companion object {
        const val SHARE_URI_EXAMPLE_1 = "j5re4cv29rip"
    }
}

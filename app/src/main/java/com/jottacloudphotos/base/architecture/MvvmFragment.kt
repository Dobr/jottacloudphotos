package com.jottacloudphotos.base.architecture

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding

abstract class MvvmFragment<VDB : ViewDataBinding, E : UiEvent, VM : BaseViewModel<E>>(
    @LayoutRes private val layoutId: Int,
    vmKClass: Class<VM>,
    viewModelVariableInt: Int,
    sharedVM: Boolean = false
) : BaseViewModelBindingFragment<VM, VDB>(
    layoutId, vmKClass, viewModelVariableInt, sharedVM
) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onFragmentCreated(savedInstanceState)
        viewModel.run {
            uiEvents.observeEvents(viewLifecycleOwner) {
                onNewUiEvent(it)
            }
            errors.observeEvents(viewLifecycleOwner) {
                showError(it)
            }
        }
    }

    abstract fun onFragmentCreated(savedInstanceState: Bundle?)

    abstract fun onNewUiEvent(uiEvent: E)

    open fun showError(error: String) {
        Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show()
        // override to change behavior
    }

}

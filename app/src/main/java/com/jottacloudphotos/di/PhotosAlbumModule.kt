package com.jottacloudphotos.di

import com.google.gson.GsonBuilder
import com.jottacloudphotos.BuildConfig
import com.jottacloudphotos.network.JottaApiService
import com.jottacloudphotos.network.data.PhotoAlbumRepository
import com.jottacloudphotos.network.data.PhotoAlbumRepositoryImpl
import com.jottacloudphotos.network.data.source.PhotoAlbumNetworkSource
import com.jottacloudphotos.network.data.source.PhotoAlbumNetworkSourceImpl
import com.jottacloudphotos.ui.overview.OverviewViewModel
import com.jottacloudphotos.ui.photos.PhotoAlbumViewModel
import com.jottacloudphotos.use_cases.PhotoAlbumUseCases
import com.jottacloudphotos.use_cases.PhotoAlbumUseCasesImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val photoAlbumModule by lazy {
    module {
        single { GsonBuilder() }
        single { get<GsonBuilder>().create() }
        single { provideDefaultOkhttpClient() }
        single { provideRetrofitBuilder(get()) }
        single { provideRetrofit(get(), get()) }
        single {
            get<Retrofit>().create(JottaApiService::class.java)
        }


        single {
            PhotoAlbumRepositoryImpl(
                get(),
            )
        } bind PhotoAlbumRepository::class

        single {
            PhotoAlbumNetworkSourceImpl(
                get(),
            )
        } bind PhotoAlbumNetworkSource::class
        viewModel { PhotoAlbumViewModel(get()) }
        viewModel { OverviewViewModel() }

        useCases()
    }
}

private fun Module.useCases() {
    single { PhotoAlbumUseCasesImpl(get()) } bind PhotoAlbumUseCases::class
}

private fun provideRetrofitBuilder(
    serializerBuilder: GsonBuilder
) = Retrofit.Builder()
    .addConverterFactory(GsonConverterFactory.create(serializerBuilder.create()))


fun provideRetrofit(
    retrofitBuilder: Retrofit.Builder,
    okHttpClient: OkHttpClient
): Retrofit {

    return retrofitBuilder.client(okHttpClient)
        .baseUrl("https://api.jottacloud.com/photos/v1/")
        .build()
}

fun provideDefaultOkhttpClient(
): OkHttpClient {
    val clientBuilder = OkHttpClient.Builder()
    if (BuildConfig.DEBUG) {
        addLoggingInterceptor(clientBuilder)
    }
    return clientBuilder.build()
}

private fun addLoggingInterceptor(clientBuilder: OkHttpClient.Builder) {
    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    clientBuilder.addInterceptor(loggingInterceptor)
}
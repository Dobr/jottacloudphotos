package com.jottacloudphotos.network.data.source

import com.jottacloudphotos.network.JottaApiService
import com.jottacloudphotos.network.model.Model

class PhotoAlbumNetworkSourceImpl(private val api: JottaApiService) :
    PhotoAlbumNetworkSource {

    override suspend fun fetchPhotoAlbum(shareUri: String): Model.Album? {
        val response = api.getAlbum(shareUri)
        when {
            response.isSuccessful -> return response.body()
            else -> throw Throwable(response.errorBody().toString())
        }
    }
}

package com.jottacloudphotos.ui.photos

enum class PhotoSize(val suffixToThumbnail: String) {
    SMALL(".s"), MEDIUM(".m"), LARGE(".l")

}

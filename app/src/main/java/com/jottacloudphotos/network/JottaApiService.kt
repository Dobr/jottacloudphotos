package com.jottacloudphotos.network

import com.jottacloudphotos.network.model.Model
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface JottaApiService {
    @GET("public/{public_share_uri}")
    suspend fun getAlbum(@Path("public_share_uri") shareUri: String): Response<Model.Album>
}
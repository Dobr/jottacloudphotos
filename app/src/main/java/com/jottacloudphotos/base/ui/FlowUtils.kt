package com.jottacloudphotos.base.architecture

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart

fun <T> Flow<T>.applyLoadingStatusInto(liveData: MutableLiveData<Boolean>): Flow<T> {
    return onStart { liveData.postValue(true) }
        .onCompletion { liveData.postValue(false) }
}

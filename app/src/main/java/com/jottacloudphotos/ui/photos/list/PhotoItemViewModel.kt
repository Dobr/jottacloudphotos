package com.jottacloudphotos.ui.photos.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData


class PhotoItemViewModel(private var photo: PhotoModel) {
    val id: String
        get() = photo.id

    val imageUrl: LiveData<String> get() = MutableLiveData(photo.thumbnailMediumUrl)

}


package com.jottacloudphotos.network.data.source

import com.jottacloudphotos.network.model.Model

interface PhotoAlbumNetworkSource {
    suspend fun fetchPhotoAlbum(shareUri: String): Model.Album?
}
